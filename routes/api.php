<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', [App\Http\Controllers\AuthController::class, 'login'])->name('login');

Route::post('register', [App\Http\Controllers\AuthController::class, 'register']);

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user', [App\Http\Controllers\AuthController::class, 'user']);
});